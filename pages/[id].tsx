import { useRouter } from 'next/router'
import { ArticleDetails } from '../components/ArticleDetails';
import useSWR, { mutate, trigger } from 'swr';



const ShowArticleDetails = () => {
  const router = useRouter()
  const { id } = router.query
  const {data} = useSWR('/'+id);
  console.log(data)
 
console.log(id)
return(<ArticleDetails data={data} id={id}/>)


}

export default ShowArticleDetails
