import {  List, ListItem, Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import useSWR from 'swr';

export function Comments({article_identifier }) {


  const [currentPage, setCurrentPage] = useState(0);
  const [currentData, setCurrentData] = useState([]);
 
  const {data} = useSWR(article_identifier+'/comments/'+currentPage);
 
  useEffect(() => {
    setCurrentData(data?.data)
  }, [data]);

 
console.log(data)

return(<List sx={{ pt: 2}}>
     <Typography gutterBottom variant="h5" component="div">
     {currentData?.length!=0 ?"All Comment Details" : null}
        </Typography>
   {currentData?.map(row => (
    <ListItem sx={{  pb:4}} key={row.id} >
{row.content}
    </ListItem> ))}
  </List>)

}


