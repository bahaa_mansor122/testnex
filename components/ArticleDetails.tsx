import { Comments } from '../components/Comments';
import { Button, Card, CardActions, CardContent, CardMedia, Typography } from '@material-ui/core';
import React from 'react';


export function ArticleDetails({data,id}) {


  return( 
    <Card  >
      <CardMedia
        component="img"
        height="300"
        width="200"
        image={data?.data?.image}
        alt="green iguana"
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
        {data?.data?.title}
        </Typography>
        <Typography variant="body2" color="text.secondary">
        {data?.data?.meta_description}
        </Typography>
      </CardContent>
      <CardActions>
      <Typography paragraph>Comment:</Typography>
          <Typography paragraph>
          {data?.data?.comments_count}
          </Typography>
          <Typography paragraph>Calps :</Typography>
          <Typography paragraph>
          {data?.data?.claps_count}
          </Typography>
       <hr/>
       <br/>
       
      </CardActions>
      <Comments article_identifier={id} />
    </Card>)
}
