import { Box, Button } from '@material-ui/core';
import React, { useState } from 'react';
import Paginator from 'react-hooks-paginator';
import useSWR, { mutate, trigger } from 'swr';

function Pagination({ currentPage,
  handlePrevPage,
  handleNextPage}) {
  

  //const {data} = useSWR('/scopes/lat/get/1');
//alert(currentPage)
  return (
    <div className="pagination-button-wrapper">
     
     &nbsp;
          <Button     onClick={() => handlePrevPage(currentPage)}
        disabled={currentPage === 0} variant="contained" color="primary">
           &larr;  Prev 
          </Button>
          &nbsp;  &nbsp;
        <span >
        Page {currentPage} 
        </span>
       &nbsp;  &nbsp;
          <Button     onClick={() => handleNextPage(currentPage)}
       variant="contained" color="primary">
           Next 	&rarr;   
          </Button>
     
    </div>
  );
}

export default Pagination;