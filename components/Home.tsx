import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
} from "@material-ui/core";
import axios from "axios";
import React, { useEffect, useState } from "react";
import useSWR, { mutate, trigger } from "swr";
import Link from "next/link";
import Pagination from "./Pagination";
import { createTheme } from "@material-ui/core/styles";

export function Home({ commentsFromServer }: any) {
  const [currentPage, setCurrentPage] = useState(0);
  const [currentData, setCurrentData] = useState([]);

  const { data } = useSWR("/scopes/lat/get/" + currentPage, {
    initialData: commentsFromServer,
  });

  const getData = async (page) => {
    let config = { "X-Tenant": "androidworld.newsifier.com" };
    const res = await axios("/scopes/lat/get/" + page, { headers: config });
    setCurrentData(res.data.data);
  };

  useEffect(() => {
    setCurrentData(data?.data);
  }, []);

  const handlePrevPage = (prevPage: number) => {
    const currentPrevPage = prevPage - 1;
    setCurrentPage((prevPage) => prevPage - 1);
    getData(currentPrevPage);
  };

  const handleNextPage = async (nextPage: number) => {
    const currentNextPage = nextPage + 1;
    setCurrentPage((nextPage) => nextPage + 1);
    getData(currentNextPage);
  };

  return (
    <React.Fragment>
      <Box
        sx={{
          display: "flex",
          flexWrap: "wrap",
          "& > :not(style)": {
            m: 1,
            width: 200,
            height: 208,
          },
        }}
      >
        {currentData?.map((row) => (
          <Card
            style={{
              marginTop: "10px",
              marginBottom: "10px",
              marginLeft: "10px",
            }}
            key={row.id}
          >
            <CardContent>
              <Typography
                sx={{ fontSize: 14 }}
                color="text.secondary"
                gutterBottom
              >
                {row.title.substring(0, 10)}
              </Typography>
              <Typography variant="h5" component="div">
                {row?.meta_description?.substring(0, 10)}
              </Typography>

              <Typography variant="body2">
                {row.type}
                <br />
                {'"a benevolent smile"'}
              </Typography>
            </CardContent>
            <CardActions>
              <Button color="primary">
                <Link href="/[id]" as={"/" + row.id}>
                  <a style={{ color: "white" }}>
                    <Typography color="primary">View More</Typography>
                  </a>
                </Link>
              </Button>
            </CardActions>
          </Card>
        ))}
      </Box>

      <Box
        marginTop={2}
        marginBottom={2}
        marginLeft={10}
        className="text-center"
      >
        <Pagination
          currentPage={currentPage}
          handlePrevPage={handlePrevPage}
          handleNextPage={handleNextPage}
        />
      </Box>
    </React.Fragment>
  );
}

Home.getInitialProps = async (ctx) => {
  let config = { "X-Tenant": "androidworld.newsifier.com" };

  const res = await axios("/scopes/lat/get/0", { headers: config });
  const json = res.data.data;
  console.log(json);
  return { commentsFromServer: json };
};
